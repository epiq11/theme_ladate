<?php

get_header();
get_template_part("template-parts/template", "hero");
get_template_part("template-parts/template", "author");
get_template_part("template-parts/template", "books");
get_template_part("template-parts/template", "crits");
get_template_part("template-parts/template", "commande");
get_footer();
